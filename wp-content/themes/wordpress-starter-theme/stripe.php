<?php
require_once (__DIR__ . "/vendor/autoload.php");
(new \Dotenv\Dotenv(__DIR__.'/'))->load();

$apiKeys = new stdClass;
$apiKeys->public = getenv('STRIPE_PUBLIC_KEY');
$apiKeys->secret = getenv('STRIPE_SECRET_KEY');

\Stripe\Stripe::setApiKey($apiKeys->secret);

add_action("wp_ajax_stripe", "stripe_callback");
add_action("wp_ajax_nopriv_stripe", "stripe_callback");
function stripe_callback() {
    $charge = \Stripe\Charge::create([
        "amount" => (int) $_POST['amount'] * 100,
        "currency" => "usd",
        "source" => "tok_mastercard", // obtained with Stripe.js
    ]);
    echo json_encode($charge);
    wp_die();
}
