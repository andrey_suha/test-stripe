<?php get_header(); ?>

	<main id="main" class="page-main" role="main">
        <div class="page-container">
            <stripe-form></stripe-form>
        </div>
	</main>

<?php get_footer(); ?>
